# Installation
The module has to be installed only on the server <br />
Add config.hpp to RPF_Server\Functions\Modules\CONFIG\moduleConfigs.hpp <br />
Add Functions.hpp to RPF_Server\Functions\Modules\CONFIG\moduleFunctions.hpp <br />
Add ServerModules_fnc_initPersistency to RPF_Server\Functions\Modules\CONFIG\fn_initModules.sqf <br />

DISABLE RESETVEHICLES FEATURE FROM GARAGE MODULE <br />

# Targets

#### Vehicles Persistency
This module will save all players' vehicles and will store them in the profileNamespace file on the server. <br />
If enabled, it will also save everything contained in the vehicles, such as backpacks,uniforms,weapons,etc.<br />

# Notes
The module DOES require Garage Module.<br />
The module ,in the worst scenario,could aggravate server performances, as it does many heavy tasks.<br />