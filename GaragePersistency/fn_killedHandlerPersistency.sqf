/*
    File : fn_killedHandlerPersistency.sqf
    Module: GaragePersistency
    Author: Dardo
    Description:
    Handle complete removal of vehicle
    Actions:
    - Removes vehicle from persistency Handler (if running)

    Arguments:
        1- Vehicle Object
*/
if (!isServer) exitWith {};

params ["_vehicle"];

_pushbackFSM = {
  params ["_handle","_var","_item"] ; 
  private _curValue = _handle getFSMVariable _var;
  if (typeName _curValue != "ARRAY") exitWith { _handle setFSMVariable [_var,[]]; };
  private _finalValue=_curValue;
  _finalValue pushBack _item;
  _handle setFSMVariable [_var,_finalValue];
};
private _vehKey = _vehicle getVariable "key";

//Check whether persistencyHandler is running,then add vehicle in queue for removal
if (!(isNil "persistencyHandler")) then {
    [persistencyHandler,"_toRemove",_vehKey] call _pushbackFSM
};